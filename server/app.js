"use strict";

const path = require("path");
const AutoLoad = require("fastify-autoload");
const cors = require("cors");
const fastify = require("fastify")();

const { db, connect } = require("./utlis/connectDB");

module.exports = async function (fastify, opts) {
  fastify.register(require("fastify-cors"), {
    origin: true,
  });
  // db.on("error", () => {
  //   console.log("Error on connection");
  // });

  // db.once("open", () => {
  //   console.log("MongoDB successfully connected");
  // });

  await connect();

  const startServer = async () => {
    try {
      await fastify.listen(5000);
    } catch (err) {
      fastify.log.error(err);
      process.exit(1);
    }
  };
  startServer();

  fastify.register(AutoLoad, {
    dir: path.join(__dirname, "routes"),
    options: Object.assign({}, opts),
  });
};
