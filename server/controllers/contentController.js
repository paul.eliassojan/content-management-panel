const content = require("../models/contentSchema");
var multer = require("multer");
var fs = require("fs");
var path = require("path");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now());
  },
});

var upload = multer({ storage: storage });

module.exports = {
  create: async (request, reply) => {
    try {
      const data = {
        title: request.body.title,
        desc: request.body.desc,
        // img: {
        //   data: fs.readFileSync(
        //     path.join(__dirname + "/uploads/" + request.file.filename)
        //   ),
        //   contentType: "image/png",
        // },
      };
      if(data.title.length>5)
      {
        const newData = await content.create(data);
        reply.code(201).send(newData);
      }
      else
      {
          reply.code(500).send("title is less than 5")
      }

    } catch (e) {
      reply.code(500).send(e);
    }
  },

  fetch: async (request, reply) => {
    try {
      const data = await content.find({});
      reply.code(200).send(data);
    } catch (e) {
      reply.code(500).send(e);
    }
  },

  get: async (request, reply) => {
    try {
      const dataId = request.params.id;
      const data = await content.findById(dataId);
      reply.code(200).send(data);
    } catch (e) {
      reply.code(500).send(e);
    }
  },
  delete: async (request, reply) => {
    try {
      const dataId = request.params.id;
      const dataDelete = await content.findById(dataId);
      await content.findByIdAndDelete(dataId);
      reply.code(200).send({ data: dataDelete });
    } catch (e) {
      reply.code(500).send(e);
    }
  },
};
