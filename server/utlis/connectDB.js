const mongoose = require("mongoose");
require('dotenv/config')
//var MongoClient = require("mongodb")
let connection = null;

async function connect() {
  connection = await mongoose.connect(process.env.MONGO_URL);
}


//const db = MongoClient.connect;

module.exports = {  connect };
