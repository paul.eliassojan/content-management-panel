const mongoose = require('mongoose');
const { Schema } = mongoose;
  
const contentSchema= new Schema({
    title:{
        type: String,
        required:true
    },
    desc:{
        type:String,
        required:true
    },
    img:
    {
        data: Buffer,
        contentType: String
    },
});
contentSchema.set('timestamps',true)
module.exports=mongoose.model('content',contentSchema);