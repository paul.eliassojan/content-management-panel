"use strict";
const { getCollection, getCoordinates } = require("../utlis/connectDB");
const contentController = require("../controllers/contentController");

module.exports = async function (fastify, opts) {
  fastify.get("/", contentController.fetch);
  fastify.post("/add", contentController.create);
  fastify.get("/:id", contentController.get);
  fastify.delete("/:id", contentController.delete);
};
