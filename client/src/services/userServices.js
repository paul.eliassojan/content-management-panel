import axios from "axios";
const API_URL = "http://127.0.0.1:5000/";

class UserService {
  ContentData() {
    return axios.get(API_URL).then((res) => res.data);
  }

  PostContent(ImageData) {
    return axios.post(API_URL + "add", ImageData).then((res) => res.data);
  }
}
export default new UserService();
