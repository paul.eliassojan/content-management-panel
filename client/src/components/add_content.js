import React, { useEffect, useState } from "react";
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  Button,
  IconButton,
  TextField,
  Grid,
  TextareaAutosize,
} from "@mui/material";
import Cancel from "@mui/icons-material/Close";
import UserService from "../services/userServices";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AddContent = () => {
  let navigate = useNavigate();

  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const routeChange = () => {
    let path = `/`;
    navigate(path);
  };
  const handleSave = async () => {
    if (title != "" && desc != "") {
      const obj = {
        title: title,
        desc: desc,
      };

      UserService.PostContent(obj)
        .then((res) => {
          let path = `/`;
          navigate(path);
        })
        .catch((error) => {
          toast.warn("Title length less than 5", {
            position: "bottom-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        });
    } else {
      toast.warn("Please fill all fields", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };
  return (
    <div>
      <ToastContainer />

      <Box sx={{ flexGrow: 1 }}>
        <AppBar style={{ backgroundColor: "#ebebeb" }} position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={routeChange}
            >
              <Cancel style={{ fontSize: "35px", color: "red" }} />
            </IconButton>
            <Typography
              style={{
                textAlign: "center",
                color: "#363636",
                fontWeight: "bold",
              }}
              variant="h6"
              component="div"
              sx={{ flexGrow: 1 }}
            >
              Add New Image
            </Typography>
            <Button onClick={handleSave} color="success" variant="contained">
              Save
            </Button>
          </Toolbar>
        </AppBar>
      </Box>
      <div style={{ marginTop: "10rem", marginLeft: "42rem" }}>
        <Grid container spacing={15}>
          <Grid item xs={10} md={8}>
            <TextField
              style={{ width: "60%" }}
              id="outlined-basic"
              label="Title"
              variant="outlined"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              required
            />
          </Grid>
          <Grid item xs={6} md={6}>
            <TextareaAutosize
              minRows={20}
              placeholder="Description"
              style={{ width: 450 }}
              value={desc}
              onChange={(e) => setDesc(e.target.value)}
              required
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default AddContent;
