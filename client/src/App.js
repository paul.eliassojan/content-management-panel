import logo from "./logo.svg";
import Content from "./components/content";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AddContent from "./components/add_content";
function App() {
  return (
      <Router>
        <Routes>
        <Route path="/" element={<Content/>}  />
        <Route path="/add-content" element={<AddContent/>} />
        </Routes>
      </Router>
  );
}

export default App;
